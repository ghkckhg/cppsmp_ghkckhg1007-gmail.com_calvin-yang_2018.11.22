#include <cstdlib>
#include <iostream>
#include <random>

using namespace std;

void question5(int ST)
{
	int doubleheadcoin = 0;
	int tenheads = 0;
	int headcount = 0;
	int i = 0, j = 0;

	/* set the random machine */
	random_device rd;
	mt19937 generator(rd());
	uniform_int_distribution<int> unif(0, 999);

	/*take out and flip coin*/
	for (i = 0; i < ST; i++)
	{
		if (unif(generator) == 0) // 0 means the double-headed coin
		{
			doubleheadcoin++;
			tenheads++;
		}
		else // 1~999 means the fair coins
		{
			// flip it 10 times
			for (j = 0; j < 10; j++)
				headcount += ( unif(generator) % 2); // 1 means head, 0 means tail

			if (headcount == 10) 
				tenheads++;
				
			headcount = 0;
		}
	}

	/*output*/
	cout << "the answer of question 5:" << endl;
	cout << "the probability is " << (float)doubleheadcoin / tenheads << endl << endl;

	/*
	Analytical solution:
	P(DH)   =1/1000		(double head coin)
	P(F)    =999/1000	(fair coins)
	P(T|DH) =1			(given DH, ten heads)
	P(T|F)  =1/1024		(given fair coins, ten heads)

	P(T)= P(T && DH)+P(T && F)
	    = P(T|DH)*P(DH)+P(T|F)*P(F)
		= 1*(1/1000) + (1/1024)*(999/1000) 
		= 1/1000 * (2023/1024)

	P(DH|T)= P(DH && T)/P(T)
		   = P(T|DH)*P(DH)/P(T)
		   = (1*1/1000)/ [1/1000 * (2023/1024)]
		   = 1024/2023
		   = 0.506
	*/
}

void question6(int ST)
{
	int* coins = new int[10000];
	int index = 0;
	int totalhead = 0;
	int totaltail = 0;
	float convergence = 0.0f;
	int i = 0;

	/* set the random machine */
	random_device rd;
	mt19937 generator( rd() );
	uniform_int_distribution<int> unif(0, 9999);

	/* generate the bag of coins */
	for (i=0; i<10000; i++)
	{
		coins[i] = unif(generator)%2; //1 means head, 0 means tail
	}

	/*robot act*/
	for (i = 0; i < ST; i++)
	{
		index = unif(generator);
		coins[index] = ( coins[index] ? unif(generator)%2 : 1 );
	}
	
	/*compute result*/
	for (i = 0; i < 10000; i++)
	{
		if (coins[i] == 1)
			totalhead++;
		else
			totaltail++;
	}	

	/*output*/
	cout << "the answer of question 6:" << endl;
	cout << "total head is " << totalhead << endl;
	cout << "total tail is " << totaltail << endl;

	convergence = (float)totalhead / totaltail;
	cout << "the convergence of head/tail is " << convergence << endl << endl;

	/*delete*/
	delete coins;
}

void question7(int ST)
{
	double dayprofit = 0.0;
	int profitabovezero = 0;
	int i = 0, j=0;

	/* set the random machine */
	random_device rd;
	mt19937 generator( rd() );
	normal_distribution<double> norm( 15000.0, 100000.0 );

	/*trade by John*/
	for (i = 0; i < ST; i++)
	{
		for (j = 0; j < 255; j++)
		{
			dayprofit += norm(generator);
		}

		if (dayprofit > 0)
			profitabovezero++;

		dayprofit = 0.0;
	}

	/*output*/
	cout << "the answer of question 7:" << endl;
	cout << "the probability that above zero is " << (float)profitabovezero/ST << endl;
}

int main()
{
	int SimulationTimes = 100000;
	
	question5(SimulationTimes);
	question6(SimulationTimes);
	question7(SimulationTimes);

	//system("pause");
	return 0;
}
