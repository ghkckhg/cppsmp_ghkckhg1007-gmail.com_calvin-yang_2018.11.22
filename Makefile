CC = g++
CFLAGS = -O3 
INC = -I ./include
LIB = -L ./lib

all: 
	${CC} mainParent.cpp childA.cpp childB.cpp ${CFLAGS} ${INC} ${LIB} -o mainParent
	
clean:                             
	@rm -rf *.o mainParent