#include <sys/wait.h> 
#include <unistd.h> 
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cerrno> 

using namespace std;

pthread_mutex_t shm_mutex = PTHREAD_MUTEX_INITIALIZER;

extern int childA_pipe(int *, pid_t);
extern int childB_shm(int);

int parentRoutine(int fdSet[], int shmParentId, int *shmBuffer, pid_t childA)
{
	float userInput=0.0f;
	int userInputInt=0;
	int* sendBuffer=NULL;
	int serialNumber=1, i=0;
	int len=0;
	int status=0;
	int ret=0;
	
	sendBuffer = new int[4096];
	close(fdSet[0]); 
	
	/* random seed */
	srand(time(NULL));

	while(1)
	{
		usleep(10 * 1000); //wait 10ms for synchronization
		
		cout << endl << endl << "[mainParent] Enter a positive integer or 0 to exit: ";
		cin >> userInput;
		
		if((floor(userInput) != userInput) || userInput < 0) //validate user input
		{
			cout << "[mainParent] Can only accept positive integers, please try again." << endl;
			continue;
		}
		else
			userInputInt = (int) userInput;
		
		if(userInputInt != 0)
			cout << "[mainParent] Generating " << userInputInt << " random integers: ";
		
		/*       self-defined format       */
		/*   0   1    2     3        n-1   */
		/* |seq|len|data0|data1|...|dataN| */
		
		sendBuffer[0] = serialNumber; 
		
		if(!userInputInt)
			sendBuffer[1] = -1; //if 0 to exit, fill -1 to announce all processes
		else
			sendBuffer[1] = userInputInt;
		
		for(i = 2; i < userInputInt + 2; i++) //generate N random numbers and fill them to array and print to screen
		{	
			sendBuffer[i] = (rand()%(100-50+1)) + 50;
			cout << sendBuffer[i] << " ";
		}
		
		cout << endl;
		
		/* communicate using pipe to child A */
		ret = write(fdSet[1], sendBuffer, sizeof(int)*(userInputInt + 2));
		
		/* communicate using shared memory to child B */
		pthread_mutex_lock(&shm_mutex);
			memcpy(shmBuffer, sendBuffer, sizeof(int)*(userInputInt + 2));
		pthread_mutex_unlock(&shm_mutex);
		
		serialNumber++; //add 1 as sequence number
		if(userInputInt == 0) //exit the program
			break;
	}
	
	cout << "[mainParent] Process Waits" << endl;
	
	/* close pipe related */
	close(fdSet[1]);
	delete sendBuffer;
	
	/* close shared momery related */
	shmdt(shmBuffer);
	shmctl(shmParentId, IPC_RMID, NULL);
	
	waitpid(childA, &status, 0);
	cout << "[mainParent] Process Exits" << endl;
	
	return 0;
}

int main()
{
	int mKey = 1234;
	int fdPipe[2];
	pid_t childA, childB;
	int* sendBuffer=NULL;	
	int shmidParent;
	
	/* create pipe */
	if (pipe(fdPipe) < 0) 
	{ 
		fprintf(stderr, "pipe: %s\n", strerror(errno)); 
		return -1;
	} 
	
	/* create shared memory */
	shmidParent = shmget(mKey, 4096*sizeof(int), 0666 | IPC_CREAT);
	sendBuffer = (int *) shmat(shmidParent, NULL, 0);
	
	if((childA = fork()) == 0)
	{
		if(childB = fork() == 0)
			childB_shm(mKey);
		else
			childA_pipe(fdPipe, childB);
	}
	else
		parentRoutine(fdPipe, shmidParent, sendBuffer, childA);
	
	return 0;
}