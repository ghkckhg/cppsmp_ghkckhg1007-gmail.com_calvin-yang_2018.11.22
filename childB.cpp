#include <sys/wait.h> 
#include <unistd.h> 
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cerrno> 

using namespace std;

extern pthread_mutex_t shm_mutex;

static void bubble_sort(int *data, int n) 
{
	for (int i = 0; i < n; ++i) 
	{
		for (int j = 0; j < i; ++j) 
		{
			if (data[j] > data[i]) 
			{
				int temp = data[j];
				data[j] = data[i];
				data[i] = temp;
			}
		}
	}
}

static float geometricMean(int *data, int n)
{
	float product = 1.0f;
	
	for(int i = 0; i < n; i++)
		product *= data[i];
	float gm = pow(product, (float) 1/n);
	return gm;
}

int childB_shm(int shmKey)
{
	int userN=0;
	int previousSerial = 0;
	int currentSerial = 0;
	int* receiveBuffer = NULL;
	int receiveLength = 0;
	int* shmRecvBuffer = NULL;
	int i = 0;

	receiveBuffer = new int[4096]; //new temp buffer
	int shmidChild = shmget(shmKey, 4096*sizeof(int), 0666 | IPC_CREAT); //get shared memory key
	shmRecvBuffer = (int *) shmat(shmidChild, NULL, 0);	//get shared memory address to access
	
	while(1)
	{
		usleep(500); //sample rate as 0.5ms
		
		pthread_mutex_lock(&shm_mutex); // get data from parent process with mutex
			currentSerial = shmRecvBuffer[0];
			userN = shmRecvBuffer[1];
			memcpy(receiveBuffer, shmRecvBuffer, sizeof(int)*(userN + 2));
		pthread_mutex_unlock(&shm_mutex);

		if(userN == -1) //if got -1, exit the process
			break;
		
		if(currentSerial == previousSerial) //to prevent duplicate data
			continue;
	
		cout << "[childB] Random Numbers Received From Shared Memory: ";
		for(i = 2; i < userN + 2; i++)
			cout << receiveBuffer[i] << " ";
		cout << endl;
		
		bubble_sort(receiveBuffer+2, userN); //sort data in increasing way
		cout << "[childB] Sorted Seqence: ";
		for(i = 2; i < userN + 2; i++)
			cout << receiveBuffer[i] << " ";
		cout << endl;
		
		cout <<  "[childB] Geometric Mean: " << geometricMean(receiveBuffer+2, userN) << endl; //do geometricMean
		previousSerial = currentSerial;
	}
	
Exit:
	shmdt(shmRecvBuffer); //dettach shared memory address
	delete receiveBuffer; //free dynamic buffer
	cout << "[childB] Child process exits" << endl;
	exit(0);
}