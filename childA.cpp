#include <sys/wait.h> 
#include <unistd.h> 
#include <sys/ipc.h>
#include <sys/shm.h>
#include <pthread.h>

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <cerrno> 

using namespace std;

static void bubble_sort(int data[], int n) 
{
	for (int i = 0; i < n; ++i) 
	{
		for (int j = 0; j < i; ++j) 
		{
			if (data[j] > data[i]) 
			{
				int temp = data[j];
				data[j] = data[i];
				data[i] = temp;
			}
		}
	}
}

static float median(int data[], int n)
{
	if(!(n%2))
		return (data[n/2-1] + data[n/2+1-1])*0.5;
	else
		return data[(n+1-1)/2];
} 

int childA_pipe(int fdSet[], pid_t childB)
{
	int userN=0;
	int* receiveBuffer = NULL;
	int receiveLength = 0;
	int status=0;
	int previousSerial = 0;
	int currentSerial = 0;
	int i = 0;

	receiveBuffer = new int[4096]; //new temp buffer
	close(fdSet[1]); //close write fd for the process

	while(1)
	{
		receiveLength = read(fdSet[0], receiveBuffer, 4096*sizeof(int)); //get data from parent process

		if (receiveLength < 0) 
		{ 
			fprintf(stderr, "read: %s\n", strerror(errno)); 
			exit(-1); 
		}
		
		currentSerial = receiveBuffer[0];
		userN = receiveBuffer[1];
		
		if(userN == -1) //if got -1, exit the process
			break;
		
		if(currentSerial == previousSerial) //to prevent duplicate data
			continue;
		
		cout << "[childA] Random Numbers Received From Pipe: ";
		for(i = 2; i < userN + 2; i++)
			cout << receiveBuffer[i] << " ";
		cout << endl;
		
		bubble_sort(receiveBuffer+2, userN); //sort data in increasing way
		cout << "[childA] Median: " << median(receiveBuffer+2, userN) << endl; //do median calculation
		
		previousSerial = currentSerial;
	}
	
Exit:
	close(fdSet[0]); //close read fd for the process
	delete receiveBuffer; //free temp buffer
	waitpid(childB, &status, 0); //wait process B exit
	cout << "[childA] Child process exits" << endl;
	exit(0);
}
